//Api url
export const LOGIN_USER_URL = "/user/auth/login";
export const LOGIN_USER_SNS_URL = "/user/auth/sns";
export const REGISTER_URL = "/user/auth/register";
export const USER_INFO_URL = "/user/info";

export const TOKEN_KEY = "token";
export const EXP_TOKEN_KEY = "exp_token";
