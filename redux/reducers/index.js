import loaduser from "redux/action/loaduser";
import loadshop from "redux/action/loadshop";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  loaduser,
  loadshop,
});

export default rootReducer;
