import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  id: "",
  code: "",
  name: "",
  intro: "",
  introVideo: "",
  phoneNumber: "",
  fbLink: "",
  instLink: "",
  youtubeLink: "",
};

const postsSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    setShop(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    createPost(state, action) {
      return state;
    },
    updatePost(state, action) {
      return state;
    },
    deletePost(state, action) {
      return state;
    },
  },
});

// Extract the action creators object and the reducer
const { actions, reducer } = postsSlice;
// Extract and export each action creator by name
export const { createPost, updatePost, deletePost, setShop } = actions;
// Export the reducer, either as a default or named export
export default reducer;
