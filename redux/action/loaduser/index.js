import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loginStatus: false,
  id: "",
  username: "",
};

const postsSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    setUser(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    clearUser(state, action) {
      return {
        ...state,
        loginStatus: false,
        id: "",
        username: "",
      };
    },
    createPost(state, action) {
      return state;
    },
    updatePost(state, action) {
      return state;
    },
    deletePost(state, action) {
      return state;
    },
  },
});

// Extract the action creators object and the reducer
const { actions, reducer } = postsSlice;
// Extract and export each action creator by name
export const {
  createPost,
  updatePost,
  deletePost,
  setUser,
  clearUser,
} = actions;
// Export the reducer, either as a default or named export
export default reducer;
