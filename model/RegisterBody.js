export const RegisterBody = (firstName, lastName, username, password) => {
  return {
    firstName: firstName,
    lastName: lastName,
    username: username,
    password: password,
  };
};
