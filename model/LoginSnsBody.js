export const LoginSnsBody = (
  authBranch,
  snsId,
  email,
  token,
  idToken,
  expiresAt,
  firstName,
  lastName,
  avatar
) => {
  return {
    authBranch: authBranch,
    snsId: snsId,
    email: email,
    token: token,
    idToken: idToken,
    expiresAt: expiresAt,
    firstName: firstName,
    lastName: lastName,
    avatar: avatar,
  };
};
