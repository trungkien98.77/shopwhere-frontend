import React from "react";
import { useSnackbar } from "notistack";
import { useRouter } from "next/router";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/nextjs-material-kit/pages/components.js";
import PageHeader from "components/PageHeader/PageHeader";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Parallax from "components/Parallax/Parallax";
import BreadcrumbsList from "../../components/Breadcrumbs/BreadcrumbsList";
import EmptyData from "../../components/EmptyData/EmptyData";

const useStyles = makeStyles(styles);
export default function MyShopPage(props) {
  const router = useRouter();
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const links = [
    { tittle: "ShopWhere.vn", path: "/" },
    { tittle: "Cửa hàng của tôi", path: "/shop/my-shop" },
    { tittle: "0 cửa hàng", path: "" },
  ];

  const handleClick = (e) => {
    e.preventDefault();
    router.push("/shop/create");
  };
  return (
    <div>
      <PageHeader
        title={"ShopWhere"}
        description={"Tìm kiếm cửa hàng thời trang tốt nhất cho bạn"}
      />
      <Header
        brand=""
        rightLinks={<HeaderLinks />}
        fixed
        color="transparent"
        changeColorOnScroll={{
          height: 250,
          color: "primary",
        }}
      />
      <Parallax
        image={require("assets/img/shop-background.jpg")}
        style={{ height: "40vh" }}
      >
        <div className={classes.container}>
          <GridContainer>
            <GridItem>
              <div className={classes.brand}>
                <h5 className={classes.title}>Cửa hàng của tôi</h5>
                <h3 className={classes.subtitle}>
                  Khởi tạo website của bạn một cách dễ dàng và miễn phí
                </h3>
              </div>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.sections}>
          <div className={classes.container}>
            <BreadcrumbsList links={links} />
            <EmptyData />
            <div className={classes.itemRight}>
              <Button color={"primary"} onClick={handleClick}>
                Tạo cửa hàng
              </Button>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
