import React from "react";
import { useSnackbar } from "notistack";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
// @material-ui/icons
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/nextjs-material-kit/pages/components.js";
import PageHeader from "components/PageHeader/PageHeader";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Parallax from "components/Parallax/Parallax";
import BreadcrumbsList from "../../components/Breadcrumbs/BreadcrumbsList";
import EmptyData from "../../components/EmptyData/EmptyData";
import ShopBasicForm from "../../components/Shop/ShopBasicForm";

const useStyles = makeStyles(styles);
export default function CreateShopPage(props) {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  const links = [
    { tittle: "ShopWhere.vn", path: "/" },
    { tittle: "Tạo cửa hàng của tôi", path: "/shop/create" },
  ];
  const [activeStep, setActiveStep] = React.useState(0);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const steps = ["Thông tin cơ bản", "Chi nhánh", "Quản trị viên"];

  function getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return <ShopBasicForm />;
      case 1:
        return "What is an ad group anyways?";
      case 2:
        return "This is the bit I really care about!";
      default:
        return "Unknown stepIndex";
    }
  }

  return (
    <div>
      <PageHeader
        title={"ShopWhere"}
        description={"Tìm kiếm cửa hàng thời trang tốt nhất cho bạn"}
      />
      <Header
        brand=""
        rightLinks={<HeaderLinks />}
        fixed
        color="transparent"
        changeColorOnScroll={{
          height: 250,
          color: "primary",
        }}
      />
      <Parallax image={require("assets/img/shop-background.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem>
              <div className={classes.brand}>
                <h5 className={classes.title}>Cửa hàng của tôi</h5>
                <h3 className={classes.subtitle}>
                  Khởi tạo website của bạn một cách dễ dàng và miễn phí
                </h3>
              </div>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>

      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.sections}>
          <div className={classes.container}>
            <BreadcrumbsList links={links} />
            <Stepper activeStep={activeStep} alternativeLabel>
              {steps.map((label) => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <div>
              {activeStep === steps.length ? (
                <div>
                  <Typography className={classes.instructions}>
                    All steps completed
                  </Typography>
                  <Button size={"sm"} onClick={handleReset}>
                    Reset
                  </Button>
                </div>
              ) : (
                <div>
                  {getStepContent(activeStep)}
                  <div>
                    <Button
                      size={"sm"}
                      disabled={activeStep === 0}
                      onClick={handleBack}
                      className={classes.backButton}
                    >
                      Back
                    </Button>
                    <Button
                      size={"sm"}
                      variant="contained"
                      color="primary"
                      onClick={handleNext}
                    >
                      {activeStep === steps.length - 1 ? "Finish" : "Next"}
                    </Button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
