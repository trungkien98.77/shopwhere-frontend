import React from "react";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import jwt_decode from "jwt-decode";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";

import styles from "assets/jss/nextjs-material-kit/pages/loginPage.js";

import image from "assets/img/source.gif";
import SocialButton from "components/Social/SocialButton";

import { EXP_TOKEN_KEY, TOKEN_KEY } from "utils/Constants";
import { LoginSnsBody } from "../model/LoginSnsBody";
import { LoginUserSnsApi } from "../api/user/LoginUserSnsApi";
import PageHeader from "../components/PageHeader/PageHeader";
import LoginForm from "../components/Auth/LoginForm";
import RegisterForm from "../components/Auth/RegisterForm";

const useStyles = makeStyles(styles);

export default function LoginPage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  const router = useRouter();
  const { forward } = router.query;
  const { enqueueSnackbar } = useSnackbar();

  const [state, setState] = React.useState("login");
  const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");

  setTimeout(function () {
    setCardAnimation("");
  }, 700);

  const handleToken = (token) => {
    let decoded = jwt_decode(token);
    console.log(decoded);
    localStorage.setItem(EXP_TOKEN_KEY, decoded.exp);
    localStorage.setItem(TOKEN_KEY, token);
  };

  const handleSocialLogin = (snsUser) => {
    console.log({ snsUser });
    LoginUserSnsApi(
      LoginSnsBody(
        snsUser._provider,
        snsUser._profile.id,
        snsUser._profile.email,
        snsUser._token.accessToken,
        snsUser._token.idToken,
        snsUser._token.expiresAt,
        snsUser._profile.firstName,
        snsUser._profile.lastName,
        snsUser._profile.profilePicURL
      )
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.status === 200) {
          handleToken(data.result.token);
          enqueueSnackbar(data.message, { variant: "success" });

          forward === undefined
            ? (window.location.href = "/")
            : (window.location.href = forward);
        } else {
          enqueueSnackbar(data.message, { variant: "warning" });
        }
      })
      .catch((reason) => {
        enqueueSnackbar(reason.message, { variant: "error" });
      });
  };

  const handleSocialLoginFailure = (err) => {
    enqueueSnackbar(err.message, { variant: "error" });
    enqueueSnackbar("Đăng nhập không thành công", { variant: "warning" });
  };
  return (
    <div>
      <PageHeader
        title={"ShopWhere-Đăng nhập"}
        description={"Tìm kiếm cửa hàng thời trang tốt nhất cho bạn"}
      />
      <Header
        absolute
        color="transparent"
        brand="NextJS Material Kit"
        rightLinks={<HeaderLinks />}
        {...rest}
      />
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center",
        }}
      >
        <div className={classes.container} style={{ paddingTop: "12vh" }}>
          <GridContainer justify="center">
            <GridItem xs={12} sm={6} md={4}>
              <Card className={classes[cardAnimaton]}>
                <CardHeader color="primary" className={classes.cardHeader}>
                  <h4>Đăng nhập - ShopWhere.vn</h4>
                  <div className={classes.socialLine}>
                    <Button
                      justIcon
                      href="#pablo"
                      target="_blank"
                      color="transparent"
                      onClick={(e) => e.preventDefault()}
                    >
                      <i className={"fab fa-twitter"} />
                    </Button>
                    <SocialButton
                      provider="facebook"
                      appId={process.env.FACEBOOK_ID}
                      onLoginSuccess={handleSocialLogin}
                      onLoginFailure={handleSocialLoginFailure}
                      justIcon
                      color="transparent"
                    >
                      <i className={"fab fa-facebook"} />
                    </SocialButton>

                    <SocialButton
                      provider="google"
                      appId={process.env.GOOGLE_ID}
                      onLoginSuccess={handleSocialLogin}
                      onLoginFailure={handleSocialLoginFailure}
                      justIcon
                      color="transparent"
                    >
                      <i className={"fab fa-google-plus-g"} />
                    </SocialButton>
                  </div>
                </CardHeader>
                {state === "login" ? (
                  <LoginForm
                    forward={forward}
                    classes={classes}
                    setState={setState}
                  />
                ) : (
                  <RegisterForm
                    forward={forward}
                    classes={classes}
                    setState={setState}
                  />
                )}
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}
