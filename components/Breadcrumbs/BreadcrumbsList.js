import React from "react";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import Primary from "components/Typography/Primary";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import Small from "components/Typography/Small";

export default function BreadcrumbsList(props) {
  const { links } = props;
  return (
    <Breadcrumbs
      separator={<NavigateNextIcon fontSize="small" />}
      aria-label="breadcrumb"
    >
      {links.map((link, index) =>
        link.path === "" ? (
          <Primary>
            <Small>{link.tittle}</Small>
          </Primary>
        ) : (
          <Primary>
            <Link color="inherit" href={link.path}>
              <Small>{link.tittle}</Small>
            </Link>
          </Primary>
        )
      )}
    </Breadcrumbs>
  );
}
