import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import empty_img from "assets/img/empty-data.png";
import styles from "assets/jss/nextjs-material-kit/pages/components.js";

const useStyles = makeStyles(styles);

export default function EmptyData() {
  const classes = useStyles();
  return (
    <div>
      <div className={classes.itemCenter}>
        <img
          style={{ width: "75%", maxWidth: 300 }}
          src={empty_img}
          alt={"empty-data"}
        />
      </div>
      <div className={classes.itemCenter}>
        <h4 className={classes.subtitle}>Không có dữ liệu!</h4>
      </div>
      <br />
    </div>
  );
}
