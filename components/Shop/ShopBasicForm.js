import { useDispatch, useSelector } from "react-redux";
import { setShop } from "redux/action/loadshop";
import React, { useState } from "react";
import InputAdornment from "@material-ui/core/InputAdornment";
import GridContainer from "../Grid/GridContainer";
import People from "@material-ui/icons/People";
import ThemeCompProvider from "../ThemeCompProvider/ThemeCompProvider";
import GridItem from "../Grid/GridItem";
import CustomInput from "../CustomInput/CustomInput";

export default function ShopBasicForm() {
  const dispatch = useDispatch();
  const shopData = useSelector((state) => state.loadshop);

  const handleChangeValue = (e) => {
    dispatch(setShop({ ...shopData, [e.target.id]: e.target.value }));
  };

  return (
    <ThemeCompProvider>
      <GridContainer>
        <GridItem xs={12} sm={4}>
          <CustomInput
            labelText="Tên cửa hàng"
            id="name"
            formControlProps={{
              fullWidth: true,
            }}
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <People />
                </InputAdornment>
              ),
              onChange: (e) => handleChangeValue(e),
              value: shopData.name,
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={9}>
          <CustomInput
            labelText="Giới thiệu cửa hàng"
            id="intro"
            formControlProps={{
              fullWidth: true,
            }}
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <People />
                </InputAdornment>
              ),
              multiline: true,
              onChange: (e) => handleChangeValue(e),
              value: shopData.intro,
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={3}>
          <CustomInput
            labelText="Link Youtube giới thiệu"
            id="introVideo"
            formControlProps={{
              fullWidth: true,
            }}
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <People />
                </InputAdornment>
              ),
              onChange: (e) => handleChangeValue(e),
              value: shopData.introVideo,
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={4} md={4} lg={3}>
          <CustomInput
            labelText="Hotline"
            id="phoneNumber"
            formControlProps={{
              fullWidth: true,
            }}
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <People />
                </InputAdornment>
              ),
              onChange: (e) => handleChangeValue(e),
              value: shopData.phoneNumber,
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={4} md={4} lg={3}>
          <CustomInput
            labelText="Trang Facebook"
            id="fbLink"
            formControlProps={{
              fullWidth: true,
            }}
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <People />
                </InputAdornment>
              ),
              onChange: (e) => handleChangeValue(e),
              value: shopData.fbLink,
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={4} md={4} lg={3}>
          <CustomInput
            labelText="Trang Instagram"
            id="instLink"
            formControlProps={{
              fullWidth: true,
            }}
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <People />
                </InputAdornment>
              ),
              onChange: (e) => handleChangeValue(e),
              value: shopData.instLink,
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={4} md={4} lg={3}>
          <CustomInput
            labelText="Kênh Youtube"
            id="youtubeLink"
            formControlProps={{
              fullWidth: true,
            }}
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <People />
                </InputAdornment>
              ),
              onChange: (e) => handleChangeValue(e),
              value: shopData.youtubeLink,
            }}
          />
        </GridItem>
      </GridContainer>
    </ThemeCompProvider>
  );
}
