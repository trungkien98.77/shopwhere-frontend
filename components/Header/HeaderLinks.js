/*eslint-disable*/
import React from 'react'

// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'

// @material-ui/icons
import { HomeWork, Forum, NotificationsActive } from '@material-ui/icons'
// core components
import Button from 'components/CustomButtons/Button.js'

import styles from 'assets/jss/nextjs-material-kit/components/headerLinksStyle.js'
import UserHeader from 'components/User/UserHeader'

const useStyles = makeStyles(styles)

export default function HeaderLinks () {
  const classes = useStyles()

  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <Button
          href="/"
          color="transparent"
          className={classes.navLink}
        >
          <HomeWork className={classes.icons}/> Trang Chủ
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          href="/"
          color="transparent"
          className={classes.navLink}
        >
          <Forum className={classes.icons}/> Chat
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          href="/"
          color="transparent"
          className={classes.navLink}
        >
          <NotificationsActive className={classes.icons}/> Thông báo
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <UserHeader/>
      </ListItem>
    </List>
  )
}
