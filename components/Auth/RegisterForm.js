import React from "react";
import { useSnackbar } from "notistack";

import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import Chip from "@material-ui/core/Chip";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import DoneIcon from "@material-ui/icons/Done";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd";

import CardBody from "components/Card/CardBody";
import Button from "components/CustomButtons/Button";
import CardFooter from "components/Card/CardFooter";
import ThemeCompProvider from "components/ThemeCompProvider/ThemeCompProvider";

import { RegisterApi } from "api/user/RegisterApi";
import { RegisterBody } from "model/RegisterBody";

const chipStyle = { fontSize: "0.7rem", color: "red", border: "none" };

export default function RegisterForm(props) {
  const { forward, classes, setState } = props;
  const { enqueueSnackbar } = useSnackbar();
  const [values, setValues] = React.useState({
    firstName: "",
    lastName: "",
    username: "",
    password: "",
    passwordCheck: "",
    showPassword: false,
  });

  const [chips, setChips] = React.useState({
    firstName: { open: false, validate: false },
    lastName: { open: false, validate: false },
    username: { open: false, validate: false },
    password: { open: false, validate: false },
    passwordCheck: { open: false, validate: false },
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleRegister = () => {
    //Check form
    if (!chips.lastName.validate) {
      setChips({ ...chips, lastName: { ...chips.lastName, open: true } });
      return;
    }
    if (!chips.firstName.validate) {
      setChips({ ...chips, firstName: { ...chips.firstName, open: true } });
      return;
    }
    if (!chips.username.validate) {
      setChips({ ...chips, username: { ...chips.username, open: true } });
      return;
    }
    if (!chips.passwordCheck.validate) {
      setChips({
        ...chips,
        password: { ...chips.password, open: true },
        passwordCheck: { ...chips.passwordCheck, open: true },
      });
      return;
    }

    //Call Api login
    RegisterApi(
      RegisterBody(
        values.firstName,
        values.lastName,
        values.username,
        values.password
      )
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 200) {
          enqueueSnackbar(data.message, { variant: "success" });
          forward === undefined
            ? (window.location.href = "/login")
            : (window.location.href = "/login?forward=" + forward);
        } else {
          enqueueSnackbar(data.message, { variant: "warning" });
        }
      })
      .catch((reason) => {
        enqueueSnackbar(reason.message, { variant: "error" });
      });
  };

  const handleOpenChip = (e) => {
    validateValues();
    let default_ = {
      firstName: { ...chips.firstName, open: false },
      lastName: { ...chips.lastName, open: false },
      username: { ...chips.username, open: false },
      password: { ...chips.password, open: false },
      passwordCheck: { ...chips.passwordCheck, open: false },
    };
    if (e.target.id === "") {
      setChips(default_);
    } else {
      setChips({
        ...default_,
        [e.target.id]: { ...chips[e.target.id], open: true },
      });
    }
  };

  const validateValues = (e) => {
    let default_ = {
      firstName: { ...chips.firstName, validate: values.firstName !== "" },
      lastName: { ...chips.lastName, validate: values.lastName !== "" },
      username: {
        ...chips.username,
        validate: validateUsername(values.username),
      },
      password: {
        ...chips.password,
        validate: validatePassword(values.password),
      },
      passwordCheck: {
        ...chips.passwordCheck,
        validate:
          values.password === values.passwordCheck && chips.password.validate,
      },
    };

    if (e) {
      switch (e.target.id) {
        case "firstName":
          setValues({ ...values, firstName: e.target.value });
          default_ = {
            ...default_,
            firstName: {
              ...default_.firstName,
              validate: e.target.value !== "",
            },
          };
          break;
        case "lastName":
          setValues({ ...values, lastName: e.target.value });
          default_ = {
            ...default_,
            lastName: {
              ...default_.lastName,
              validate: e.target.value !== "",
            },
          };
          break;
        case "username":
          setValues({ ...values, username: e.target.value.replace(" ", "") });
          default_ = {
            ...default_,
            username: {
              ...default_.username,
              validate: validateUsername(e.target.value.replace(" ", "")),
            },
          };
          break;
        case "password":
          setValues({ ...values, password: e.target.value });
          default_ = {
            ...default_,
            password: {
              ...default_.password,
              validate: validatePassword(e.target.value),
            },
            passwordCheck: {
              ...default_.passwordCheck,
              validate:
                e.target.value === values.passwordCheck &&
                validatePassword(e.target.value),
            },
          };
          break;
        case "passwordCheck":
          setValues({ ...values, passwordCheck: e.target.value });
          default_ = {
            ...default_,
            passwordCheck: {
              ...default_.passwordCheck,
              validate:
                e.target.value === values.password && chips.password.validate,
            },
          };
          break;
        default:
      }
    }
    setChips(default_);
  };

  const validateUsername = (value) => {
    const usernameRegex = /^[a-z0-9.]{3,20}$/;
    return value.match(usernameRegex);
  };

  const validatePassword = (value) => {
    const passwordRegex = /(?=(.*[0-9]))((?=.*[A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z]))^.{8,}$/;
    return value.match(passwordRegex);
  };

  const ChipValidate = (props) => {
    const { name, label } = props;
    return chips[name].open ? (
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        {chips[name].validate ? (
          <Chip
            variant="outlined"
            color="primary"
            size="small"
            label="OK"
            style={{ fontWeight: "bold" }}
            icon={<DoneIcon />}
          />
        ) : (
          <Chip
            variant="outlined"
            style={chipStyle}
            size="small"
            label={label}
          />
        )}
        <br />
      </div>
    ) : (
      <>
        <br />
        <br />
      </>
    );
  };

  return (
    <>
      <ThemeCompProvider>
        <CardBody onClick={handleOpenChip}>
          <InputLabel color={"secondary"}>Họ và tên đệm</InputLabel>
          <Input
            id="lastName"
            style={{ width: "100%" }}
            value={values.lastName}
            color={"primary"}
            onChange={(e) => validateValues(e)}
            onClick={(e) => handleOpenChip(e)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton>
                  <AssignmentIndIcon color={"primary"} />
                </IconButton>
              </InputAdornment>
            }
          />
          <ChipValidate
            name={"lastName"}
            label={"Họ và tên đệm của bạn, vd: Nguyễn Thị Minh"}
          />
          <InputLabel color={"secondary"}>Tên</InputLabel>
          <Input
            id="firstName"
            style={{ width: "100%" }}
            value={values.firstName}
            color={"primary"}
            onChange={(e) => validateValues(e)}
            onClick={(e) => handleOpenChip(e)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton>
                  <AssignmentIndIcon color={"primary"} />
                </IconButton>
              </InputAdornment>
            }
          />
          <ChipValidate name={"firstName"} label={"Tên của bạn, vd: Hải"} />
          <InputLabel color={"secondary"}>Tên đăng nhập</InputLabel>
          <Input
            id="username"
            style={{ width: "100%" }}
            value={values.username}
            color={"primary"}
            onChange={(e) => validateValues(e)}
            onClick={(e) => handleOpenChip(e)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton>
                  <AccountCircle color={"primary"} />
                </IconButton>
              </InputAdornment>
            }
          />
          <ChipValidate
            name={"username"}
            label={"3->20 ký tự gồm chữ thường, số và có thể có dấu chấm"}
          />
          <InputLabel>Mật khẩu</InputLabel>
          <Input
            id="password"
            style={{ width: "100%" }}
            type={values.showPassword ? "text" : "password"}
            value={values.password}
            onChange={(e) => validateValues(e)}
            onClick={(e) => handleOpenChip(e)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {values.showPassword ? (
                    <Visibility color={"primary"} />
                  ) : (
                    <VisibilityOff color={"primary"} />
                  )}
                </IconButton>
              </InputAdornment>
            }
          />
          <ChipValidate
            name={"password"}
            label={"Ít nhất 6 ký tự có chữ in hoa, chữ thường và số"}
          />
          <InputLabel>Nhập lại mật khẩu</InputLabel>
          <Input
            id="passwordCheck"
            style={{ width: "100%" }}
            type={values.showPassword ? "text" : "password"}
            value={values.passwordCheck}
            onChange={(e) => validateValues(e)}
            onClick={(e) => handleOpenChip(e)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {values.showPassword ? (
                    <Visibility color={"primary"} />
                  ) : (
                    <VisibilityOff color={"primary"} />
                  )}
                </IconButton>
              </InputAdornment>
            }
          />
          <ChipValidate
            name={"passwordCheck"}
            label={"Nhập lại trùng khớp với mật khẩu"}
          />
        </CardBody>
      </ThemeCompProvider>
      <CardFooter className={classes.cardFooter}>
        <Button
          simple
          color="primary"
          onClick={() => {
            setState("login");
          }}
        >
          Đăng nhập
        </Button>
        <Button
          style={{ width: "100%" }}
          color="primary"
          onClick={() => {
            handleRegister();
          }}
        >
          Đăng ký
        </Button>
      </CardFooter>
    </>
  );
}
