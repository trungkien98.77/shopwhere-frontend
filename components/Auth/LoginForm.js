import CardBody from "../Card/CardBody";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import ThemeCompProvider from "../ThemeCompProvider/ThemeCompProvider";
import React from "react";
import { LoginUserApi } from "../../api/user/LoginUserApi";
import { LoginUserBody } from "../../model/LoginUserBody";
import { useSnackbar } from "notistack";
import jwt_decode from "jwt-decode";
import { EXP_TOKEN_KEY, TOKEN_KEY } from "../../utils/Constants";
import Button from "../CustomButtons/Button";
import CardFooter from "../Card/CardFooter";

export default function LoginForm(props) {
  const { forward, classes, setState } = props;
  const { enqueueSnackbar } = useSnackbar();
  const [values, setValues] = React.useState({
    username: "",
    password: "",
    showPassword: false,
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleToken = (token) => {
    let decoded = jwt_decode(token);
    console.log(decoded);
    localStorage.setItem(EXP_TOKEN_KEY, decoded.exp);
    localStorage.setItem(TOKEN_KEY, token);
  };

  const handleLogin = () => {
    //Check form
    if (values.username === "" || values.password === "") {
      enqueueSnackbar("Tên đăng nhập hoặc mật khẩu không được nhập", {
        variant: "warning",
      });
      return;
    }
    //Call Api login
    LoginUserApi(LoginUserBody(values.username, values.password))
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.status === 200) {
          handleToken(data.result.token);
          enqueueSnackbar(data.message, { variant: "success" });

          forward === undefined
            ? (window.location.href = "/")
            : (window.location.href = forward);
        } else {
          enqueueSnackbar(data.message, { variant: "warning" });
        }
      })
      .catch((reason) => {
        enqueueSnackbar(reason.message, { variant: "error" });
      });
  };

  return (
    <>
      <p className={classes.divider}>Or Be Classical</p>
      <ThemeCompProvider>
        <CardBody>
          <InputLabel color={"secondary"}>Tên đăng nhập</InputLabel>
          <Input
            style={{ width: "100%" }}
            value={values.username}
            color={"primary"}
            onChange={handleChange("username")}
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                handleLogin();
              }
            }}
            endAdornment={
              <InputAdornment position="end">
                <IconButton>
                  <AccountCircle color={"primary"} />
                </IconButton>
              </InputAdornment>
            }
          />
          <br />
          <br />
          <InputLabel>Mật Khẩu</InputLabel>
          <Input
            style={{ width: "100%" }}
            type={values.showPassword ? "text" : "password"}
            value={values.password}
            onChange={handleChange("password")}
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                handleLogin();
              }
            }}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {values.showPassword ? (
                    <Visibility color={"primary"} />
                  ) : (
                    <VisibilityOff color={"primary"} />
                  )}
                </IconButton>
              </InputAdornment>
            }
          />
        </CardBody>
      </ThemeCompProvider>
      <CardFooter className={classes.cardFooter}>
        <Button
          style={{ width: "100%" }}
          onClick={() => {
            handleLogin();
          }}
          color="primary"
        >
          Đăng nhập
        </Button>
        <Button
          simple
          color="primary"
          onClick={() => {
            setState("sign");
          }}
        >
          Đăng ký
        </Button>
      </CardFooter>
    </>
  );
}
