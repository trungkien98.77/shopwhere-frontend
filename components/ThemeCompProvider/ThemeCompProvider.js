import React from "react";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import {
  primaryColor,
  secondaryColor,
} from "../../assets/jss/nextjs-material-kit";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: primaryColor,
    },
    secondary: {
      main: secondaryColor,
    },
  },
});

const ThemeCompProvider = ({ children }) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export default ThemeCompProvider;
