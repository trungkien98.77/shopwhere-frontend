import React, { useState, useEffect } from "react";
import Link from "next/link";
import { makeStyles } from "@material-ui/core/styles";
import CustomDropdown from "components/CustomDropdown/CustomDropdown";
import styles from "assets/jss/nextjs-material-kit/pages/componentsSections/navbarsStyle.js";
import { EXP_TOKEN_KEY, TOKEN_KEY } from "utils/Constants";
import Button from "components/CustomButtons/Button";
import { GetUserInfoApi } from "api/user/GetUserInfoApi";
import { setUser, clearUser } from "redux/action/loaduser";
import { useDispatch } from "react-redux";

import profileImage from "assets/img/acc.png";
const useStyles = makeStyles(styles);

export default function UserHeader() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [userState, setUserState] = useState({
    loginStatus: false,
    id: "",
    username: "",
    firstName: "",
    lastName: "",
    email: "",
    avatar: "",
    address: "",
    phoneNumber: "",
  });

  useEffect(() => {
    GetUserInfoApi()
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 200) {
          dispatch(
            setUser({
              loginStatus: true,
              id: data.result.id,
              username: data.result.username,
            })
          );
          setUserState({ ...userState, ...data.result, loginStatus: true });
        } else {
          setUserState({ ...userState, loginStatus: false });
          dispatch(clearUser());
        }
      })
      .catch(() => {
        setUserState({ ...userState, loginStatus: false });
        dispatch(clearUser());
      });
  }, []);

  const handleLogout = () => {
    if (typeof window !== "undefined") {
      localStorage.setItem(EXP_TOKEN_KEY, null);
      localStorage.setItem(TOKEN_KEY, null);
      window.location.reload();
    }
  };

  return (
    <>
      {userState.loginStatus ? (
        <CustomDropdown
          left
          caret={false}
          dropdownHeader={userState.username}
          buttonText={
            <img
              src={
                userState.avatar === null || userState.avatar === ""
                  ? profileImage
                  : userState.avatar
              }
              className={classes.img}
              alt="profile"
            />
          }
          buttonProps={{
            className: classes.navLink + " " + classes.imageDropdownButton,
            color: "transparent",
          }}
          dropdownList={[
            <Link href="/user/profile">
              <a className={classes.black}>Thông tin tài khoản</a>
            </Link>,
            <Link href="/shop/my-shop">
              <a className={classes.black}>Cửa hàng của Tôi</a>
            </Link>,
            <div onClick={handleLogout} className={classes.black}>
              Đăng xuất
            </div>,
          ]}
        />
      ) : (
        <Button href="/login" color="primary" round>
          Đăng nhập
        </Button>
      )}
    </>
  );
}
