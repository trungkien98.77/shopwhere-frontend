import { REGISTER_URL } from "utils/Constants";

export const RegisterApi = (body) => {
  let url = process.env.SERVER_URL + REGISTER_URL;
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body),
  };
  return fetch(url, requestOptions);
};
