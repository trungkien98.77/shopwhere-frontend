import { LOGIN_USER_URL } from "utils/Constants";

export const LoginUserApi = (body) => {
  let url = process.env.SERVER_URL + LOGIN_USER_URL;
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body),
  };
  return fetch(url, requestOptions);
};
