import { EXP_TOKEN_KEY, TOKEN_KEY, USER_INFO_URL } from "utils/Constants";

export const GetUserInfoApi = () => {
  let url = process.env.SERVER_URL + USER_INFO_URL;
  let token;
  let expToken;

  if (typeof window !== "undefined") {
    token = localStorage.getItem(TOKEN_KEY);
    expToken = localStorage.getItem(EXP_TOKEN_KEY);
  }

  const requestOptions = {
    method: "GET",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  };

  const checkValidValue = (value) => {
    return !(
      value === undefined ||
      value === null ||
      value === "" ||
      value === "undefined" ||
      value === "null"
    );
  };

  const checkToken = () => {
    if (!checkValidValue(token)) {
      return false;
    }
    return checkValidValue(expToken) && expToken > Date.now() / 1000;
  };

  if (checkToken()) {
    return fetch(url, requestOptions);
  } else {
    return new Promise(function (resolve, reject) {
      throw "Token không tồn tại hoặc đã hết hạn";
    });
  }
};
