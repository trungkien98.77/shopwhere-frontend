import { LOGIN_USER_SNS_URL } from "utils/Constants";

export const LoginUserSnsApi = (body) => {
  let url = process.env.SERVER_URL + LOGIN_USER_SNS_URL;
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body),
  };
  return fetch(url, requestOptions);
};
