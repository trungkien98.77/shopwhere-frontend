import { container, primaryColor } from "assets/jss/nextjs-material-kit.js";

const componentsStyle = {
  container,
  brand: {
    color: primaryColor,
    textAlign: "right",
  },
  title: {
    fontSize: "2.2rem",
    fontWeight: "bold",
    position: "relative",
  },
  subtitle: {
    fontSize: "1.313rem",
    margin: "10px 0 0",
    color: primaryColor,
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3",
  },
  mainRaised: {
    minHeight: "560px",
    margin: "-85px 100px 0px",
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
    "@media (max-width: 830px)": {
      marginLeft: "10px",
      marginRight: "10px",
    },
  },
  link: {
    textDecoration: "none",
  },
  textCenter: {
    textAlign: "center",
  },
  itemCenter: {
    display: "flex",
    justifyContent: "center",
  },
  itemLeft: {
    display: "flex",
    justifyContent: "flex-start",
  },
  itemRight: {
    display: "flex",
    justifyContent: "flex-end",
  },
};

export default componentsStyle;
